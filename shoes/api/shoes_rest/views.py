from django.http import JsonResponse
from django.shortcuts import render
from .models import Shoe, BinVO
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
import json


# Create your views here.
class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = ["closet_name","import_href", "id"]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ["manufacturer", "model_name", "color", "picture_url","bin"]

    encoders = {
        "bin": BinVOEncoder(),
    }

class ShoeDetailEncoder(ModelEncoder):
    model= Shoe
    properties = ["manufacturer", "model_name", "color", "picture_url", "bin",]

    encoders = {
        "bin": BinVOEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "POST":
        content = json.loads(request.body)
        try:
            bin_href = content["bin"]  #its getting the content that we input in insomnia when creating a ashoe
            bin = BinVO.objects.get(import_href=bin_href) #makesures to get the correct match that we input 
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    else:
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes" : shoes},
            encoder = ShoeListEncoder,
        )

@require_http_methods(["GET", "DELETE"])
def api_show_shoe(request, pk):
    if request.method == "GET":
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    else:
        count, _ = Shoe.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
