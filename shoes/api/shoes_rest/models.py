from django.db import models

# Create your models here.

#needed to talk between microservices
class BinVO(models.Model):
    closet_name = models.CharField(max_length=100)
    import_href = models.CharField(max_length=200, unique=True)


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=30, null = True, blank=True)
    model_name = models.CharField(max_length=30, null = True, blank=True)
    color = models.CharField(max_length=30, null = True, blank=True)

    picture_url = models.URLField(null=True)

    bin = models.ForeignKey(
        BinVO,
        related_name = "bins",
        on_delete =models.PROTECT,
    )
