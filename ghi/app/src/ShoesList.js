import React, {useState, useEffect } from 'react';

function ShoeList() {
    const [shoes, setShoes] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/shoes/';
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          setShoes(data.shoes);
        }
      }

    useEffect(() => {
        fetchData();
      }, []);

    return(
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Manufacturer</th>
                    <th>Model Name</th>
                    <th>Color</th>
                    <th>Picture</th>
                    <th>Bin</th>
                </tr>
            </thead>
            <tbody>
            {shoes.map(shoe => {
                return (
                    <tr key={shoe.id}>
                        <td>{ shoe.manufacturer }</td>
                        <td>{ shoe.model_name }</td>
                        <td>{ shoe.color }</td>
                        <td>{ shoe.picture_url }</td>
                        <td>{ shoe.bin.closet_name}</td>
                    </tr>
                );
            })}
            </tbody>
        </table>
    );
}
export default ShoeList;
